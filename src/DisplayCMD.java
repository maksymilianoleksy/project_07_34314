import java.util.Random;

public class DisplayCMD {
    public void logo(){
        System.out.println(
                " #     #                                                     \n" +
                        " ##   ## # #      #  ####  #    # ###### #####  ###### #   # \n" +
                        " # # # # # #      # #    # ##   # #      #    #     #   # #  \n" +
                        " #  #  # # #      # #    # # #  # #####  #    #    #     #   \n" +
                        " #     # # #      # #    # #  # # #      #####    #      #   \n" +
                        " #     # # #      # #    # #   ## #      #   #   #       #   \n" +
                        " #     # # ###### #  ####  #    # ###### #    # ######   #   ");
    }

    public void buttonSelect(String mess, int select){
        System.out.println("["+select+"]"+mess);
    }

    private String win(boolean is, int money){
        return is?"*"+money:" "+money;
    }
    private String textToBlock(int lenghtBox, String text){
        String textBox="";
        int roznica=lenghtBox-text.length();
        if (roznica>0){
            String space = new String(new char[roznica]).replace('\0', ' ');
            textBox=text+space;
        }else {
            textBox=text.substring(0, lenghtBox-1);
        }
        return textBox;
    }
    public void buttonQUIZ(int money, String question, String ansA, String ansB, String ansC, String ansD, boolean publicznosc, boolean polowa, boolean telefon){
        boolean [] wins=new boolean[12];
        String pub= publicznosc?" % % % % ":" ------- ";
        String pol= polowa?"  50/50   ":" ------- ";
        String tel= telefon?"  +48     ":" ------- ";
        if (money>=0 && money<12){
            wins[money]=true;
        }
        System.out.println(
                "E=>wyjście                                             12  "+win(wins[11], 1000000)+"  \n" +
                        "*****************************************              11   "+win(wins[10], 500000)+"  \n" +
                        "*"+textToBlock(39, question) +"*              10   "+win(wins[9], 250000)+"   \n" +
                        "*                                       *               9   "+win(wins[8], 125000)+"   \n" +
                        "*****************************************               8    "+win(wins[7], 75000)+"   \n" +
                        "                                                        7    "+win(wins[6], 40000)+"   \n" +
                        " ********************   ********************            6    "+win(wins[5], 20000)+"   \n" +
                        " *A"+textToBlock(17, ansA)+"*   *B"+textToBlock(17, ansB)+"*            5    "+win(wins[4], 10000)+"   \n" +
                        " ********************   ********************            4      "+win(wins[3], 5000)+"  \n" +
                        " ********************   ********************            3      "+win(wins[2], 2000)+"  \n"+
                        " *C"+textToBlock(17, ansC)+"*   *D"+textToBlock(17, ansD)+"*            2      "+win(wins[1], 1000)+"  \n" +
                        " ********************   ********************            1       "+win(wins[0], 500)+"  \n"+
                        " ************  ************    ************                         \n" +
                        " *1"+pub  +"*  *2"+pol  +"*   *3"+tel   +"*                        \n" +
                        " ************  ************    ************                         \n");

    }

    public String helpPublicznosc(Questions q){
        Random query=new Random();
        int lose=74;
        int pula=1;
        int indexWinnerQuery=0;
        int [] qsave=new int[4];
        for (int i=0;i<4;i++){
            if (q.checkAnserw(i)) indexWinnerQuery=i;
            else {
                qsave[i]=query.nextInt(lose);
                lose-=qsave[i];
                pula+=qsave[i];
            }
        }
        qsave[indexWinnerQuery]=100-pula;
        return "|"+q.getAnserw(0)+qsave[0]+"%|"+" "+q.getAnserw(1)+qsave[1]+"%|"+q.getAnserw(2)+qsave[2]+"%|"+" "+ q.getAnserw(1)+qsave[3]+"%|";
    }
    public String helpTelefon(Questions q){
        int cloop=0;
        while (!q.checkAnserw(cloop)){
            cloop++;
        }
        return q.getAnserw(cloop)+"\n";
    }
    public int [] helpHalf(Questions q){
        Random query=new Random();
        int [] pula= {-1, -1};
        int ileWPuli=0;
        int index;
        while (ileWPuli<2){
            index=query.nextInt(3);
            if (!q.checkAnserw(index)){
                if ((pula[0]>=0 && pula[1]!=pula[0]) || pula[0]<0) {
                    pula[ileWPuli] = index;
                    ileWPuli++;
                }
            }
        }
        return pula;
    }

    public void resultGame(int points){
        int moneywins=0;
        if (points==12) moneywins=1000000;
        else if (points>=7) moneywins=40000;
        else if (points>=2) moneywins=1000;
        System.out.println(
                "  ########################################################   \n" +
                        "  ###         GRATULUJE WYGRALES                       ###   \n" +
                        "  ########################################################   \n" +
                        "  ###                                                  ###   \n" +
                        "  ###           "+textToBlock(34, moneywins+"zl")+"     ###   \n" +
                        "  ###                                                  ###   \n" +
                        "  ########################################################   ");
    }
}
