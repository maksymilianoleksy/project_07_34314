public class Questions {
    private Anserw [] anserws;
    private String question;

    public Questions(String question, String [] ans , int correctAns) {
        this.question = question;

        if(ans.length!=4){
            throw new IllegalArgumentException("odpowiedzi ma być tylko 4. nie mniej nie więcej");
        }
        if(correctAns>=4 || correctAns<1){
            throw new IllegalArgumentException("odpowiedzi jest cztery. Masz wybrać więc z przedziału 1 a 4");
        }
        this.anserws = new Anserw[4];
        for(int i=0;i<4;i++){
            this.anserws[i]=new Anserw();
            this.anserws[i].setAns(ans[i]);

        }
        this.anserws[correctAns-1].setCorrect(true);
    }
    public Questions(String question, String answers1, String answers2, String answers3, String answers4 , int correctAns) {
        this.question = question;
        String [] ans={answers1, answers2, answers3, answers4};

        if(correctAns>=4 || correctAns<1){
            throw new IllegalArgumentException("odpowiedzi jest cztery. Masz wybrać więc z przedziału 1 a 4");
        }
        this.anserws = new Anserw[4];
        for(int i=0;i<4;i++){
            this.anserws[i]=new Anserw();
            this.anserws[i].setAns(ans[i]);

        }
        this.anserws[correctAns-1].setCorrect(true);
    }

    public String getQuestion() {
        return question;
    }
    public void setAnserws(int index, String other){
        if(index>3 || index<0){
            throw new IllegalArgumentException("odpowiedzi jest cztery. Masz wybrać więc z przedziału 0 a 3");
        }
        anserws[index].setAns(other);
    }
    public String getAnserw(int index) {
        if(index>3 || index<0){
            throw new IllegalArgumentException("odpowiedzi jest cztery. Masz wybrać więc z przedziału 0 a 3");
        }
        return anserws[index].getAns();
    }
    public Boolean checkAnserw(int index) {
        if(index>3 || index<0){
            throw new IllegalArgumentException("odpowiedzi jest cztery. Masz wybrać więc z przedziału 0 a 3");
        }
        return anserws[index].isCorrect();
    }
}
class Anserw{
    private String ans;
    private boolean correct;

    public Anserw() {
        this.ans ="";
        this.correct = false;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public String getAns() {
        return ans;
    }

    public boolean isCorrect() {
        return correct;
    }
}
