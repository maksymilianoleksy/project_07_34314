

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class MainTest {
    private static DisplayCMD cmd;
    private static Scanner user;

    private static void mainMenu() throws Exception {
        int choice=0;
        while (choice!=2) {
            cmd.logo();
            cmd.buttonSelect("Start", 1);
            cmd.buttonSelect("Wyjdź", 2);
            choice= user.nextInt();
            clearConsole();
            if (choice==1){
                game();
            }
        }
    }
    private static int [] randomIndex(){
        Random random=new Random();
        ArrayList <Integer> elements=new ArrayList<Integer>();
        elements.add(0);
        elements.add(1);
        elements.add(2);
        elements.add(3);
        int select;
        int [] r =new int[4];
        for (int i= elements.size();i>0;i--){
            select=random.nextInt(i);
            r[4-i]= elements.get(select);
            elements.remove(select);
        }
        return r;
    }
    private static int wygrana;
    private static void game() throws Exception {
        String choice;
        boolean [] pdg={true, true, true};
        wygrana=0;
        int [] helper={-1, -1};
        boolean exit= false;
        int questionid=0;

        ReaderCSV quest = new ReaderCSV();


        int [] indexAns=randomIndex();
        while (!exit) {
            Questions questions= quest.getQuestions()[questionid];
            cmd.logo();
            cmd.buttonQUIZ(wygrana, (wygrana+1)+". "+questions.getQuestion(),
                    questions.getAnserw(indexAns[0]), questions.getAnserw(indexAns[1]), questions.getAnserw(indexAns[2]),
                    questions.getAnserw(indexAns[3]),
                    pdg[0], pdg[1], pdg[2] );
            choice= user.next();
            clearConsole();
            choice=choice.toUpperCase();
            switch (choice.charAt(0)){
                case 'A':
                    if (questions.checkAnserw(indexAns[0])){
                        if (wygrana>=11){
                            System.out.println("gratulacje, Wygrałeś");
                            resultGame();
                            exit=true;
                        }else {
                            questionid++;
                            wygrana++;
                            indexAns=randomIndex();
                        }
                    }else {
                        if (indexAns[0]!=helper[0] && indexAns[0]!=helper[1]) {
                            resultGame();
                            exit = true;
                        }
                    }
                    break;
                case 'B':
                    if (questions.checkAnserw(indexAns[1])){
                        if (wygrana>=11){
                            System.out.println("gratulacje, Wygrałeś");
                            resultGame();
                            exit=true;
                        }else {
                            questionid++;
                            wygrana++;
                            indexAns=randomIndex();
                        }
                    }else {
                        if (indexAns[0]!=helper[0] && indexAns[0]!=helper[1]) {
                            resultGame();
                            exit = true;
                        }
                    }
                    break;
                case 'C':
                    if (questions.checkAnserw(indexAns[2])){
                        if (wygrana>=11){
                            System.out.println("gratulacje, Wygrałeś");
                            resultGame();
                            exit=true;
                        }else {
                            questionid++;
                            wygrana++;
                            indexAns=randomIndex();
                        }
                    }else {
                        if (indexAns[0]!=helper[0] && indexAns[0]!=helper[1]) {
                            resultGame();
                            exit = true;
                        }
                    }
                    break;
                case 'D':
                    if (questions.checkAnserw(indexAns[3])){
                        if (wygrana>=11){
                            System.out.println("gratulacje, Wygrałeś");
                            resultGame();
                            exit=true;
                        }else {
                            questionid++;
                            wygrana++;
                            indexAns=randomIndex();
                        }
                    }else {
                        if (indexAns[0]!=helper[0] && indexAns[0]!=helper[1]) {
                            resultGame();
                            exit = true;
                        }
                    }
                    break;
                case '1':
                    if (pdg[0]){
                        System.out.println();
                        pdg[0]=false;
                        System.out.println(cmd.helpPublicznosc(questions));
                    }
                    break;
                case '2':
                    if (pdg[1]){
                        pdg[1]=false;
                        helper= cmd.helpHalf(questions);
                        questions.setAnserws(helper[0], "----------");
                        questions.setAnserws(helper[1], "----------");
                    }
                    break;
                case '3':
                    if (pdg[2]){
                        pdg[2]=false;
                        System.out.println(cmd.helpTelefon(questions));
                    }
                    break;
                case 'E':
                    exit=true;
                    resultGame();
                    break;
            }


        }
    }
    private static void resultGame(){
        cmd.resultGame(wygrana+1);
        String nvm= user.next();

    }
    public static void main(String[] args) {
        user= new Scanner(System.in);
        cmd=new DisplayCMD();
        try {
            mainMenu();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public static void clearConsole() {
        try {
            final String os = System.getProperty("os.name");

            if (os.contains("Windows")) {
                Runtime.getRuntime().exec("cls");
            } else {
                Runtime.getRuntime().exec("clear");
            }
        }
        catch (final Exception e){
            System.out.println(e.getMessage());
        }
    }
}
