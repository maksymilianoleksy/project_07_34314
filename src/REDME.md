Zespół:
-Daniel Zych
-Maksymilian Oleksy

                NISP
                Dokumentacja


I. Sformułowanie zadania projektowego.


    Głównym celem projektu jest stworzenie popularnego teleturnieju Milionerzy . 
Użytkownik odpowiada na  serię pytań w grze.
 Będzie można jej użyć w celach rozrywkowych lub w formie testu w zależności 
od przygotowanych pytań.

CELE:

Sprawdzanie wiedzy użytkownika lub zapewnienie rozrywki



II. Analiza stanu wyjściowego

Aplikacja jest projektowana i tworzona od początku, dla klientów
 chcących sprawdzić swoją wiedzę . Pytania są przechowywane za pomocą plików CSV.


     
III. Analiza wymagań użytkownika
-użytkownik-może korzystać z dowolnych funkcji jakie udostępnia aplikacja. Gra

IV. Określenie scenariuszy użytkowania i
przypadków użycia


Rozgrywka
aktor:użytkownik
-Przypadek rozpoczyna się gdy użytkownik rozpocznie grę
Aplikacja w sposób losowy wybiera plik CSV z pytaniami.
Użytkownik wybiera odpowiedź spośród 4 dostępnych i przechodzi dalej 
Aplikacja weryfikuje czy odpowiedz jest prawidlowa 
i zezwala przejście do następnego pytania aż do określonego 
limitu ewentualnie po błędnej odpowiedzi kończy grę





Zakończenie gry
aktor:użytkownik
-Przypadek rozpoczyna się gdy użytkownik odpowiedział na z
 góry ustalona ilość pytań lub zakończył grę poprzez wpisanie litery E
-system wyświetla wynik
-użytkownik może zakończyć działanie aplikacji lub rozpocząć ponownie






V. Identyfikacja funkcji.


DisplayCMD-klasa posiadająca metody do wypisywania scenerii za pomocą funkcji
Questions-klasa posiadająca klasę Anserw przechowująca odpowiedzi 
w formie String oraz boolean odpowiadający za sprawdzenie poprawności odpowiedzi.
 Poza Anserw posiada takze zapisana w tresci pytanie oraz metody za zwracanie 
treści pytań i odpowiedzi oraz poprawności.

ReaderCSV-klasa odpowiedzialna za ładowanie do klasy Questions
 pytań zapisanego w jednym z plików csv (pierwsza odpowiedź jest zawsze poprawna w pliku)

MainTest- Główna klasa w której przechowywane są metody main,
 menu, game i result. main odpowiada za wywołanie metody menu, 
która pozwala wyjść lub rozwiązać grę. Game w której zawarto mechanikę gry.

