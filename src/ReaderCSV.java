import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Random;

public class ReaderCSV {


    private Questions[] questions;



    public ReaderCSV() throws Exception{

            int min =0;
            int max = 2;

            Random random = new Random();

            int num = random.nextInt(max + min) + min;
        String path="src\\pytaniaCSV\\pytania"+num+".csv";
        String line="";
        this.questions =new Questions[12];
        try {
            BufferedReader br=new BufferedReader(new FileReader(path));
            int index=0;
            while ((line=br.readLine())!= null){
                String[] values=line.split(",");
                questions[index]=new Questions(values[0], values[1], values[2],values[3], values[4], 1 );
                index++;
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }

    }

    public Questions[] getQuestions() {
        return questions;
    }
}